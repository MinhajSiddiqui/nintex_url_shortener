import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import {Observable} from "rxjs/Observable";
import 'rxjs/add/operator/map'
import { ShrinkURLDTO } from '../DTO/ShrinkURLDTO';
import { ResponseDTO } from '../DTO/ResponseDTO';

@Injectable()
export class ShrinkUrlService {

  data: ShrinkURLDTO;

  constructor(private http: Http) {
    this.data = new ShrinkURLDTO();
  }

  getShrinkedURL(longURL: string): Observable<ResponseDTO>{
    let headers = new Headers();
    headers.append("Content-type", "application/json");

    this.data.long_url = longURL;
    //TODO: Get real domain name.
    return this.http.post("shrinkURL",
      this.data,
      {
        headers: headers
      })
      .map(res => {
        return res.json();
      });
  }

}
