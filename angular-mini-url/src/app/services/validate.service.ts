import { Injectable } from '@angular/core';

@Injectable()
export class ValidateService {

  constructor() { }

  validateRegister(longURL: string): boolean {
    if(longURL == undefined || longURL.length == 0){
      return false;
    }

    return true;
  }


  validateURL(longURL: string): boolean {
    try{
      var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
      var regex = new RegExp(expression);

      if (longURL.match(regex)) {
        return true;
      }

      return false;
    }catch(exception){
      return false;
    }
  }
}
