import { Component, OnInit } from '@angular/core';
import { ValidateService } from  '../../services/validate.service';
import { ShrinkUrlService } from  '../../services/shrink-url.service';
import { FlashMessagesService } from 'angular2-flash-messages';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  longURL: string;
  isShortURLAvailable: boolean = false;
  shortURL: string = undefined;
  isGeneratingURL: boolean = false;

  constructor(
    private validateService: ValidateService,
    private shrinkURLService: ShrinkUrlService,
    private flashMessagesService: FlashMessagesService
  ) {

  }

  ngOnInit() {

  }

  onURLGeneratorSubmit(){
    console.log(this.longURL);

    if(!this.validateInput(this.longURL)){
      this.flashMessagesService
        .show("Please enter valie url", {cssClass: 'alert-danger'});
      return;
    }

    // If everything works fine then proceed to next step and call
    // Node service to generate mini URL
    //TODO: Make sure this suscriber is unsubscribe once done its work.
    this.isGeneratingURL = true;
    this.shrinkURLService.getShrinkedURL(this.longURL).subscribe(
      response => {
        if(response.success == undefined || !response.success){
          this.flashMessagesService
            .show(response.message, {cssClass: 'alert-danger'})
        } else {
          console.log(response);
          this.isShortURLAvailable = true;
          this.isGeneratingURL = false;
          this.shortURL = response.message;
        }

        this.isGeneratingURL = false;
      }
    )
  }

  validateInput(url: string): boolean{
    // If URL Entered.
    if(!this.validateService.validateRegister(url.trim())){
      // TODO: Show error message
      console.log("Invalid URL");
      return false;
    }

    // If URL is a valid URL
    if(!this.validateService.validateURL(url.trim())){
      //TODO: Show error message
      console.log("Invalid URL");
      return false;
    }

    return true;
  }

}
