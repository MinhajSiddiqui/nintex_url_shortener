/**
 * Created by mani on 14/05/2017.
 */
export interface ResponseDTO{
  success: boolean;
  status : number,
  message: string
}
