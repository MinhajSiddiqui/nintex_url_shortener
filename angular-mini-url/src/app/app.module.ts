import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';


import { ValidateService } from './services/validate.service';
import { ShrinkUrlService } from './services/shrink-url.service';

import { FlashMessagesModule } from 'angular2-flash-messages';

const  appRoutes: Routes =[
  { path: '', component: HomeComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FlashMessagesModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [ ValidateService, ShrinkUrlService ],
  bootstrap: [AppComponent]
})

export class AppModule { }
