import { AngularMiniUrlPage } from './app.po';

describe('angular-mini-url App', () => {
  let page: AngularMiniUrlPage;

  beforeEach(() => {
    page = new AngularMiniUrlPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
