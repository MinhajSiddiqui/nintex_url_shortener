/**
 * Created by mani on 14/05/2017.
 */
const express = require('express');
const router  = express.Router();
var metaInfo = require("../meta.js");

var bodyParser = require('body-parser');
var shortid = require('shortid');
var mongodb = require('../configs/mongodb.js');
var urlInfoModel = require('../models/url_shortener_model');

// parse application/json
router.use(bodyParser.json());

// Shrink URL
router.route('/shrinkURL')
    .post(function(req, res) {

        console.log(req.body);

        // Parse data for URL
        var urlInfo = req.body;

        if(urlInfo.long_url == undefined){
            metaInfo.message = "No url found";
            metaInfo.status = 400;
            metaInfo.success = false;

            res.json(metaInfo);
            return;
        }

        // if there was some data then check if db is connected
        mongodb.mongodbConnection(function (connection) {

            if(!connection) {
                metaInfo.message = "Unable to connect db";
                metaInfo.status = 500;
                metaInfo.success = false;

                return;
            }

            console.log("DB is connected on post!");

            const query = { "long_url" : urlInfo.long_url },
                  shortID = shortid.generate();

            urlInfoModel.find(
                query,
                function (error, result) {

                console.log(error);
                console.log(result);

                if(error != null){
                    metaInfo.message = error;
                    metaInfo.status = 500;
                    metaInfo.success = false;

                    res.json(metaInfo);
                    return;
                }

                if(result != null && result.length > 0) {
                    metaInfo.message = req.protocol + '://' + req.get('host') +"/"+ result[0].short_id;
                    metaInfo.status = 200;
                    metaInfo.success = true;

                    res.json(metaInfo);
                    return;
                }

                  const newURL = new urlInfoModel();
                  newURL.short_id = shortID;
                  newURL.long_url = urlInfo.long_url;

                  newURL.save(function (error) {
                   if(error != null){
                        metaInfo.message = error;
                        metaInfo.status = 500;
                        metaInfo.success = false;

                        res.json(metaInfo);
                        console.log("3rd");
                        return;
                    }

                    metaInfo.message = req.protocol + '://' + req.get('host') +"/"+ shortID;
                    metaInfo.status = 200;
                    metaInfo.success = true;

                    res.json(metaInfo);
                    return;
                    });
            });
        });


    });

// Get real url from shortURL
router.route('/:shortid')
    .get(function(req, res) {
        urlInfoModel.findOne({
            'short_id':req.params.shortid
            },
            function(err, result) {
            if (err) {
                // Redirect to some error page rather.
                res.json(err);
            }

            res.redirect(result.long_url);
        });
    });




module.exports = router;