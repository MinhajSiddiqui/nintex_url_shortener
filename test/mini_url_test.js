/**
 * Created by mani on 14/05/2017.
 */

const express = require("express");
var urlInfoModel = require('../models/url_shortener_model');

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//long_url schema definition
const  LongURLInfo = new Schema(
    {
        short_id: { type: String, required: true },
        long_url: { type: String, required: true }
    },
    {
        versionKey: false
    }
);

// Sets the longURL parameter equal to the current time
LongURLInfo.pre('save', function(next) {
    this.long_url = "https://www.google.com/search?q=google+images+cyberjaya&safe=" +
        "strict&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjYopaZ4u_" +
        "TAhXCQo8KHYPCCicQ_AUICigB&biw=1321&bih=776#imgrc=wenyuyukPUMktM:";

    next();
});

//Exports the BookSchema for use elsewhere.
module.exports = mongoose.model('book', LongURLInfo);

