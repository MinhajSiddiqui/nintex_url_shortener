/**
 * Created by mani on 13/05/2017.
 */
var mongoose = require("mongoose");

var connectionString = "mongodb://mani:nintex123@ds157559.mlab.com:57559/url_shortener";

module.exports = {
    mongodbConnection: function ConnectDB(connection) {

    if(mongoose.connection.readyState){
        return connection(true);
    }

    // connect to our database
    mongoose.connect(connectionString);

    var db = mongoose.connection;

    db.on('error',
        function() {
        console.error.bind(console, 'connection error:');
            return false;
        }
    );

    db.once('open', function() {
        console.log("DB Connected !");
        return false;
    });

    }
};