/**
 * Created by mani on 13/05/2017.
 */
var mongoose = require('mongoose');
var Schema       = mongoose.Schema;

var url_info_schema = new Schema({
    short_id: { type: String, required: true },
    long_url: { type: String, required: true }
});

module.exports = mongoose.model('UrlInfo', url_info_schema,'url_info');




