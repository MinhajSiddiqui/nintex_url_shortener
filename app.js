/**
 * Created by mani on 14/05/2017.
 */
/**
 * Created by mani on 13/05/2017.
 */
const express    = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
var mongodb = require('./configs/mongodb.js');
const app = express();
const shrinkURL = require('./routes/shrink_url');

const port = process.env.PORT || 8080;

// ROUTES FOR OUR API
var router = express.Router();
// ******************************************************** //


// Set angular files.
app.use(express.static(path.join(__dirname, 'public')));
// ******************************************************** //


// body parser json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// ******************************************************** //

app.use(cors());

// ******************************************************** //
app.use(shrinkURL);
// ******************************************************** //

// REGISTER OUR ROUTES
app.use('/',  router);
// ******************************************************** //


// Start Server// and connect to DB.
app.listen(port, function(){
    console.log("App running at port - "+ port);
    mongodb.mongodbConnection();
});
// ******************************************************** //